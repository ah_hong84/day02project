//Load express and path
var express = require("express");
var path = require("path");

//Create an instance of express app
var app = express();

var imgUrls = ["giraffe.jpg","whale_shark_picture.jpg"];

//Define routes
app.use(express.static(path.join(__dirname, "/public")));
app.use("/pictures", express.static(path.join(__dirname, "/image")));

app.use(function(req, resp){
    var random_num = parseInt(Math.random() * imgUrls.length);
    var htmlstring = "<img src='"+ imgUrls[random_num] + "'>";
    
    resp.status(404);
    resp.type("text/html"); //Respesentation
    resp.send(htmlstring);
    console.log("Random number: " + random_num);
});

// config the port 
app.set("port", parseInt(process.argv[2]) || parseInt(process.env.APP_PORT) || 3000);

//
console.log(">> port: %d", app.get("port"));

// 
app.listen(app.get("port"), function(){
    console.log("Application started at port %d", app.get("port"))
});